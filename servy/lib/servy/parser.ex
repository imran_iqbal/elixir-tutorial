defmodule Servy.Parser do
  alias Servy.Conv

  def parse(request) do
    [top, param_string] = String.split(request, "\r\n\r\n")

    [request_line | headers] = String.split(top, "\r\n")

    [method, path, _] = String.split(request_line, " ")

    headers = parse_headers(headers, %{})
    params = parse_params(headers["Content-Type"], param_string)

    %Conv{method: method, path: path, params: params, headers: headers}
  end

  def parse_params("application/x-www-form-urlencoded", param_string) do
    param_string |> String.trim() |> URI.decode_query()
  end

  def parse_params(_, _), do: %{}

  def parse_headers([head | tail], headers) do
    [key, value] = String.split(head, ": ")
    headers = Map.put(headers, key, value)
    parse_headers(tail, headers)
  end

  def parse_headers([], headers), do: headers
end
