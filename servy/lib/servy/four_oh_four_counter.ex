defmodule Servy.FourOhFourCounter do
  @name __MODULE__

  use GenServer

  defmodule State do
    defstruct missed_counts: %{}
  end

  # Client functions
  def start do
    IO.puts("Starting the 404 server..")
    GenServer.start(__MODULE__, %State{}, name: @name)
  end

  def bump_count(path) do
    GenServer.cast(@name, {:bump_count, path})
  end

  def get_count(path) do
    GenServer.call(@name, {:get_count, path})
  end

  def get_counts() do
    GenServer.call(@name, :get_counts)
  end

  # Server

  def init(state) do
    {:ok, state}
  end

  def handle_cast({:bump_count, path}, state) do
    old_counts = state.missed_counts
    old_count = Map.get(old_counts, path, 0)
    new_counts = Map.put(old_counts, path, old_count + 1)
    {:noreply, %{state | missed_counts: new_counts}}
  end

  def handle_call({:get_count, path}, _from, state) do
    {:reply, Map.get(state.missed_counts, path, 0), state}
  end

  def handle_call(:get_counts, _from, state) do
    {:reply, state.missed_counts, state}
  end
end
