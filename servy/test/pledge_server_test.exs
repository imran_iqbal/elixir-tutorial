defmodule PledgeServerTest do
  use ExUnit.Case

  alias Servy.PledgeServer

  test "Pledge server only caches 3 most recent pledges" do
    PledgeServer.start()

    PledgeServer.create_pledge("tony1", 10)
    PledgeServer.create_pledge("tony2", 11)
    PledgeServer.create_pledge("tony3", 12)
    PledgeServer.create_pledge("tony4", 13)
    PledgeServer.create_pledge("tony5", 14)

    assert PledgeServer.recent_pledges() == [{"tony5", 14}, {"tony4", 13}, {"tony3", 12}]

    assert PledgeServer.total_pledged() == 14 + 13 + 12
  end
end
